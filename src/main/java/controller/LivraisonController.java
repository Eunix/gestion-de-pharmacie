/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import connexion.ConnexionDB;
import static connexion.ConnexionDB.ConnexionDB;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Commande;
import model.Livraison;

/**
 *
 * @author eunix
 */
public class LivraisonController {
    Connection connection; 
    ConnexionDB connexionDB;
    PreparedStatement preparedStatement;
    Statement statement;
    Commande commande;
    
    public LivraisonController(){
     connection = (Connection) ConnexionDB();
    }
    
    
    public void insert(Livraison livraison){
        String query = "INSERT INTO Livraison (id,num_livraison,qte_liv,date_liv,num_comde) VALUES (?,?,?,?,?)";
        try {
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, livraison.getId());
            preparedStatement.setString(2, livraison.getNumLivraison());
            preparedStatement.setLong(3, livraison.getQteLiv());
            preparedStatement.setDate(4, livraison.getDateLiv());
            preparedStatement.setString(5, livraison.getNumCmde());
            preparedStatement.executeUpdate();
            
            preparedStatement.close();
            connection.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(LivraisonController.class.getName()).log(Level.SEVERE, null, ex.getMessage());
        }
    }
    
    public Long getLastLivraison(){   
        Long id =0L;
        String query = " SELECT max(id_liv) FROM livraison"; 
         try { 
             statement = connection.createStatement();            
             ResultSet resultSet = statement.executeQuery(query);
             while (resultSet.next()){
                  id = resultSet.getLong(1);                  
             }
             connection.close();            
         } catch (SQLException ex) {
             Logger.getLogger(LivraisonController.class.getName()).log(Level.SEVERE, null, ex.getMessage());
         }
         return id;
    }

    public void update(Livraison livraison){
        String query = "UPDATE Livraison SET id=?, num_livraison=?, qte_liv=?, date_liv=?, num_comde=? where id_liv=?";
         try { 
             preparedStatement = connection.prepareStatement(query);
             preparedStatement.setLong(1, livraison.getId());
             preparedStatement.setString(2, livraison.getNumLivraison());
             preparedStatement.setLong(3, livraison.getQteLiv());
             preparedStatement.setDate(4, livraison.getDateLiv());
             preparedStatement.setString(5, livraison.getNumCmde());
             preparedStatement.setLong(6, livraison.getIdLiv());
             preparedStatement.executeUpdate();
             
             preparedStatement.close();
             connection.close();
         } catch (SQLException ex) {
             Logger.getLogger(LivraisonController.class.getName()).log(Level.SEVERE, null, ex.getMessage());
         }
    }
    
    public void delete(long id){
        String query = "Delete FROM Livraison WHERE id_liv=?";
         try {
             preparedStatement = connection.prepareStatement(query);
             preparedStatement.setLong(1, id);
             preparedStatement.executeUpdate();
             
             preparedStatement.close();
             connection.close(); 
         } catch (SQLException ex) {
             Logger.getLogger(LivraisonController.class.getName()).log(Level.SEVERE, null, ex.getMessage()); 
         }
    }
    
    public List<Livraison> getAllLivraison(){
        List<Livraison> livrList= new ArrayList<>();
        String query = " SELECT l.id_liv, l.id, l.num_livraison, l.date_liv, l.qte_liv, l.num_comde, m.designation from Livraison l, Medicament m, Medicamentliv ml where l.id_liv=ml.ID_LIV and m.id_med=ml.ID_MED"; 
         try { 
             statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(query);
              while (resultSet.next()){
                 Livraison livraison =new Livraison(resultSet.getLong(1),resultSet.getLong(2),resultSet.getString(3),resultSet.getDate(4),resultSet.getLong(5),resultSet.getString(6),resultSet.getString(7));
                  livrList.add(livraison);             
              }           
         } catch (SQLException ex) {
             Logger.getLogger(LivraisonController.class.getName()).log(Level.SEVERE, null, ex.getMessage());
         }
         return livrList;
    }  
}
