/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;


import connexion.ConnexionDB;
import static connexion.ConnexionDB.ConnexionDB;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Medicament;

/**
 *
 * @author eunix
 */
public class MedicamamentController {
    Connection connection; 
    ConnexionDB connexionDB;
    PreparedStatement preparedStatement;
    Statement statement;
    Medicament medicament; 
    
    public MedicamamentController(){
        connection = (Connection) ConnexionDB();
    }
    
    public void store (Medicament medicament){
        String query = "INSERT INTO Medicament(designation,type,prix_med) VALUES(?,?,?)";
        try {
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, medicament.getDesignation());
            preparedStatement.setString(2, medicament.getType());
            preparedStatement.setFloat(3, medicament.getPrix());
            preparedStatement.executeUpdate();
            
            preparedStatement.close();
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(MedicamamentController.class.getName()).log(Level.SEVERE, null, ex.getMessage());
        }
    }
    
    public List<Medicament> index(){
        List<Medicament> listMed= new ArrayList<>();
        String query =" SELECT * FROM Medicament";
        try {
            preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
               medicament = new Medicament(resultSet.getLong(1),resultSet.getString(2),resultSet.getString(3),resultSet.getFloat(4)) ; 
                listMed.add(medicament);
            }
            preparedStatement.close();
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(MedicamamentController.class.getName()).log(Level.SEVERE, null, ex.getMessage());
        }
        return listMed;
    }
    
    public Long last(){       
        Long id =0L;
        String query = "SELECT max(id_med) FROM medicament"; 
         try { 
            statement = connection.createStatement();            
             ResultSet resultSet = statement.executeQuery(query);
             while (resultSet.next()){
                  id = resultSet.getLong(1);                  
             }
             
            statement.close();
            connection.close();
         } catch (SQLException ex) {
             Logger.getLogger(MedicamamentController.class.getName()).log(Level.SEVERE, null, ex.getMessage());
         }
         return id;
    }
    
    public List<Medicament> search(String Nom){     
        List<Medicament> listMed= new ArrayList<>();
        String query = "SELECT * FROM Medicament WHERE designation LIKE '%"+Nom+"%'";
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()){
               medicament = new Medicament(resultSet.getLong(1),resultSet.getString(2),resultSet.getString(3),resultSet.getFloat(4)) ; 
                listMed.add(medicament);
            }
            
            statement.close();
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(MedicamamentController.class.getName()).log(Level.SEVERE, null, ex.getMessage());
        }       
        return listMed;
    }
 
    public Medicament showByName(String designation){
        medicament = new Medicament();
        String query = "SELECT * FROM Medicament WHERE designation='"+designation+"'";
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()){
                medicament = new Medicament(resultSet.getLong(1),resultSet.getString(2),resultSet.getString(3),resultSet.getFloat(4)); 
            }
            
            resultSet.close();
            statement.close();
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(MedicamamentController.class.getName()).log(Level.SEVERE, null, ex.getMessage());
        }
        return medicament;       
    }
    
    public Medicament showById(long id){        
        String query = " SELECT * FROM Medicament WHERE id_med=?";
        try {
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.first()){
                medicament=new Medicament(resultSet.getLong(1),resultSet.getString(2),resultSet.getString(3),resultSet.getFloat(4));
            }
            
            resultSet.close();
            preparedStatement.close();
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(MedicamamentController.class.getName()).log(Level.SEVERE, null, ex.getMessage());
        }
        
        return medicament;
    }

    public void update(Medicament medicament){
        String query = "UPDATE Medicament SET designation=?,type=?,prix_med=? WHERE id_med=?";
        try {
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, medicament.getDesignation());
            preparedStatement.setString(2, medicament.getType());
            preparedStatement.setFloat(3,medicament.getPrix());
            preparedStatement.setLong(4,medicament.getIdMed());      
            preparedStatement.executeUpdate();
            
            preparedStatement.close();
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(MedicamamentController.class.getName()).log(Level.SEVERE, null, ex.getMessage());
        }
    }

    public void delete(long id){
        String query = "DELETE FROM Medicament WHERE id_med=?"; 
        try { 
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();

            preparedStatement.close();
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(MedicamamentController.class.getName()).log(Level.SEVERE, null, ex.getMessage());
        }   
    }
}
