/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import connexion.ConnexionDB;
import static connexion.ConnexionDB.ConnexionDB;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Pharmacien;

/**
 *
 * @author eunix
 */
public class PharmacienController {
    Connection connection;       
    ConnexionDB dbcon;
    String requete; 
    PreparedStatement preparedStatement;
    Statement statement;
    Pharmacien pharmacien;

    public PharmacienController() {
       connection = (Connection) ConnexionDB();
    }
    
    public void insert(Pharmacien pharmacien){
        String query = "INSERT INTO pharmacien(nom, prenom, fonction) VALUES (?,?,?)";
        try {
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, pharmacien.getNom());
            preparedStatement.setString(2, pharmacien.getPrenom());
            preparedStatement.setString(3, pharmacien.getFonction());
            preparedStatement.executeUpdate();
            
            preparedStatement.close();
            connection.close();      
        } catch (SQLException e) {
            Logger.getLogger(PharmacienController.class.getName()).log(Level.SEVERE, null, e.getMessage());
        }
    }
    
    public List<Pharmacien> index(){
        List<Pharmacien> pharmcienList= new ArrayList<>();
        String query="SELECT id, nom, prenom, fonction FROM pharmacien"; 
         try { 
             statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(query);
              while (resultSet.next()){
                  pharmacien=new Pharmacien(resultSet.getLong(1),resultSet.getString(2),resultSet.getString(3),resultSet.getString(4));
                  pharmcienList.add(pharmacien);   
              }
              connection.close();
         } catch (SQLException ex) {
             Logger.getLogger(PharmacienController.class.getName()).log(Level.SEVERE, null, ex.getMessage());
         }
         return pharmcienList;
    }
    
    public Pharmacien showByName(String nom){
        String query ="SELECT id, nom, prenom, fonction FROM pharmacien WHERE nom= '"+nom+"'"; 
         try { 
             statement = connection.createStatement();
             ResultSet result = statement.executeQuery(query);
               if (result.next()){
                   pharmacien=new Pharmacien(result.getLong(1),result.getString(2),result.getString(3),result.getString(4));
               }
               
               statement.close();
               connection.close();
         } catch (SQLException ex) {
             Logger.getLogger(PharmacienController.class.getName()).log(Level.SEVERE, null, ex.getMessage());
         }
        return pharmacien;
    }
    
    public Pharmacien showById(Long id){      
        String query = "SELECT id, nom, prenom, fonction FROM pharmacien WHERE id=?"; 
         try { 
             preparedStatement = connection.prepareStatement(requete);
             preparedStatement.setLong(1, id);
             ResultSet result = preparedStatement.executeQuery();
               if (result.first()){
                   pharmacien=new Pharmacien(result.getLong(1),result.getString(2),result.getString(3),result.getString(4));
               }
               
               preparedStatement.close();
               connection.close();
         } catch (SQLException ex) {
             Logger.getLogger(PharmacienController.class.getName()).log(Level.SEVERE, null, ex.getMessage());
         }
        return pharmacien;
    }
    
    public void update(Pharmacien pharmacien){
        String query =" Update pharmacien SET nom=?,prenom=?,fonction=? WHERE id=?";
         try { 
             preparedStatement = connection.prepareStatement(query);
             preparedStatement.setString(1, pharmacien.getNom());
             preparedStatement.setString(2, pharmacien.getPrenom());
             preparedStatement.setString(3, pharmacien.getFonction());
             preparedStatement.setLong(4, pharmacien.getId());           
             preparedStatement.executeUpdate(); 
             
             preparedStatement.close();
            connection.close();
         } catch (SQLException ex) {
             Logger.getLogger(PharmacienController.class.getName()).log(Level.SEVERE, null, ex.getMessage());
         }
    }
    
    public void delete(long id){
        String query = "DELETE FROM pharmacien WHERE id=?";
         try {
             preparedStatement = connection.prepareStatement(query);
             preparedStatement.setLong(1, id);
             preparedStatement.executeUpdate();
             
             preparedStatement.close();
            connection.close();
         } catch (SQLException ex) {
             Logger.getLogger(PharmacienController.class.getName()).log(Level.SEVERE, null, ex.getMessage());
         }
    }
}
