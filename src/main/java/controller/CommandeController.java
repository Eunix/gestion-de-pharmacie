/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import static connexion.ConnexionDB.ConnexionDB;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.MedicamentCommander;

/**
 *
 * @author eunix
 */
public class CommandeController {
    Connection connection; 
    PreparedStatement preparedStatement;
    MedicamentCommander commande;

    public CommandeController() {
        connection = (Connection) ConnexionDB();
    }
   
    public void insert(MedicamentCommander command){        
        String query = "INSERT INTO medicamentcom(id_med,id_cmde) VALUES(?,?)";
        try { 
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, command.getIdMed());
            preparedStatement.setLong(2, command.getIdCmde());
            preparedStatement.executeUpdate();
            
            preparedStatement.close();
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(CommandeController.class.getName()).log(Level.SEVERE, null, ex.getMessage());
        }
    }
    
    public void update(MedicamentCommander command){
        String query = "UPDATE medicamentcom SET ID_MED=? where ID_CMDE=?";
         try { 
             preparedStatement = connection.prepareStatement(query);
             preparedStatement.setLong(1, command.getIdMed());
             preparedStatement.setLong(2, command.getIdCmde());
                preparedStatement.executeUpdate(); 
                
                preparedStatement.close();
                connection.close();
             
         } catch (SQLException ex) {
             Logger.getLogger(CommandeController.class.getName()).log(Level.SEVERE, null, ex.getMessage());
         }
    }
    
    public void delete(long id){
        String query ="Delete FROM Medicamentcom WHERE id_cmde=?";
         try {
             preparedStatement = connection.prepareStatement(query);
             preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
            
            preparedStatement.close();
            connection.close();
         } catch (SQLException ex) {
             Logger.getLogger(CommandeController.class.getName()).log(Level.SEVERE, null, ex.getMessage());
         }
    }
    
}
