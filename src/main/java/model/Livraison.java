/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Date;

/**
 *
 * @author eunix
 */
public class Livraison {
    private long idLiv; 
    private long id; 
    private long idFour;
    private String numLivraison; 
    private Date dateLiv; 
    private long qteLiv; 
    private Date dateSaisie; 
    private String numCmde; 
    private String med;

    public Livraison() {
    }

    
    public Livraison(long idLiv, long id, long idFour, String numLivraison, Date dateLiv, long qteLiv, Date dateSaisie, String numCmde) {
        this.idLiv = idLiv;
        this.id = id;
        this.idFour = idFour;
        this.numLivraison = numLivraison;
        this.dateLiv = dateLiv;
        this.qteLiv = qteLiv;
        this.dateSaisie = dateSaisie;
        this.numCmde = numCmde;
    }

    public Livraison(long id, long idFour, String numLivraison, Date dateLiv, long qteLiv, Date dateSaisie, String numCmde) {
        this.id = id;
        this.idFour = idFour;
        this.numLivraison = numLivraison;
        this.dateLiv = dateLiv;
        this.qteLiv = qteLiv;
        this.dateSaisie = dateSaisie;
        this.numCmde = numCmde;
    }

    public Livraison(long idLiv, long id, String numLivraison, Date dateLiv, long qteLiv, String numCmde, String med) {
        this.idLiv = idLiv;
        this.id = id;
        this.numLivraison = numLivraison;
        this.dateLiv = dateLiv;
        this.qteLiv = qteLiv;
        this.numCmde = numCmde;
        this.med = med;
    }

    
    
    public Livraison(long id, String numLivraison, Date dateLiv, long qteLiv, String numCmde) {
        this.id = id;
        this.numLivraison = numLivraison;
        this.dateLiv = dateLiv;
        this.qteLiv = qteLiv;
        this.numCmde = numCmde;
    }

    public Livraison(long idLiv, long id, String numLivraison, Date dateLiv, long qteLiv, String numCmde) {
        this.idLiv = idLiv;
        this.id = id;
        this.numLivraison = numLivraison;
        this.dateLiv = dateLiv;
        this.qteLiv = qteLiv;
        this.numCmde = numCmde;
    }
    
    

    public long getIdLiv() {
        return idLiv;
    }

    public void setIdLiv(long idLiv) {
        this.idLiv = idLiv;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getIdFour() {
        return idFour;
    }

    public void setIdFour(long idFour) {
        this.idFour = idFour;
    }

    public String getNumLivraison() {
        return numLivraison;
    }

    public void setNumLivraison(String numLivraison) {
        this.numLivraison = numLivraison;
    }

    public Date getDateLiv() {
        return dateLiv;
    }

    public void setDateLiv(Date dateLiv) {
        this.dateLiv = dateLiv;
    }

    public long getQteLiv() {
        return qteLiv;
    }

    public void setQteLiv(long qteLiv) {
        this.qteLiv = qteLiv;
    }

    public Date getDateSaisie() {
        return dateSaisie;
    }

    public void setDateSaisie(Date dateSaisie) {
        this.dateSaisie = dateSaisie;
    }

    public String getNumCmde() {
        return numCmde;
    }

    public void setNumCmde(String numCmde) {
        this.numCmde = numCmde;
    }

    public String getMed() {
        return med;
    }

    public void setMed(String med) {
        this.med = med;
    }
    @Override
    public String toString() {
        return "Livraison{" + "idLiv=" + idLiv + ", id=" + id + ", idFour=" + idFour + ", numLivraison=" + numLivraison + ", dateLiv=" + dateLiv + ", qteLiv=" + qteLiv + ", dateSaisie=" + dateSaisie + ", numCmde=" + numCmde + '}';
    }
    
}
