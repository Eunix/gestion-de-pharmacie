/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author eunix
 */
public class Fournisseur {
    
    private long idFour; 
    private String nomFour; 
    private String prenomFour; 
    private String numCarte; 
    private String nomEntp;

    public Fournisseur() {
    }
    
    public Fournisseur(long idFour, String nomFour, String prenomFour, String numCarte, String nomEntp) {
        this.idFour = idFour;
        this.nomFour = nomFour;
        this.prenomFour = prenomFour;
        this.numCarte = numCarte;
        this.nomEntp = nomEntp;
    }

    public Fournisseur(String nomFour, String prenomFour, String numCarte, String nomEntp) {
        this.nomFour = nomFour;
        this.prenomFour = prenomFour;
        this.numCarte = numCarte;
        this.nomEntp = nomEntp;
    }
    
     public long getIdFour() {
        return idFour;
    }

    public void setIdFour(long idFour) {
        this.idFour = idFour;
    }

    public String getNomFour() {
        return nomFour;
    }

    public void setNomFour(String nomFour) {
        this.nomFour = nomFour;
    }
    
     public String getPrenomFour() {
        return prenomFour;
    }

    public void setPrenomFour(String prenomFour) {
        this.prenomFour = prenomFour;
    }

    public String getNumCarte() {
        return numCarte;
    }

    public void setNumCarte(String numCarte) {
        this.numCarte = numCarte;
    }

    public String getNomEntp() {
        return nomEntp;
    }
    
    public void setNomEntp(String nomEntp) {
        this.nomEntp = nomEntp;
    }
    @Override
    public String toString() {
        return "Fournisseur{" + "idFour=" + idFour + ", nomFour=" + nomFour + ", prenomFour=" + prenomFour + ", numCarte=" + numCarte + ", nomEntp=" + nomEntp + '}';
    }
    


    
}
