/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author eunix
 */
public class MedicamentVendu {
    private long idMed; 
    private long idVente; 

    public MedicamentVendu(long idMed, long idVente) {
        this.idMed = idMed;
        this.idVente = idVente;
    }

    public MedicamentVendu() {
    }

    public long getIdMed() {
        return idMed;
    }

    public void setIdMed(long idMed) {
        this.idMed = idMed;
    }

    public long getIdVente() {
        return idVente;
    }

    public void setIdVente(long idVente) {
        this.idVente = idVente;
    }
    @Override
     public String toString() {
        return "MedicamentVente{" + "idMed=" + idMed + ", idVente=" + idVente + '}';
    }
}
