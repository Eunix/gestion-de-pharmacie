/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Date;

/**
 *
 * @author eunix
 */
public class Vente {
    private long idVente; 
    private long qteVdu; 
    private Date dateVente; 
    private String RdateVente;
    private float montantVente;
    private String med_designation;

    public Vente() {
    }


    public Vente(long idVente, long qteVdu, String  RdateVente, float montantVente, String med_designation) {
        this.idVente = idVente;
        this.qteVdu = qteVdu;
        this.RdateVente = RdateVente;
        this.montantVente = montantVente;
        this.med_designation = med_designation;
    }

    public String getMed_designation() {
        return med_designation;
    }

    public void setMed_designation(String med_designation) {
        this.med_designation = med_designation;
    }

    public Vente(long idVente, long qteVdu, Date dateVente, float montantVente) {
        this.idVente = idVente;
        this.qteVdu = qteVdu;
        this.dateVente = dateVente;
        this.montantVente = montantVente;
    }
    
    
    public Vente(long qteVdu, Date dateVente, long montantVente) {
        this.qteVdu = qteVdu;
        this.dateVente = dateVente;
        this.montantVente = montantVente;
    }

    public long getIdVente() {
        return idVente;
    }

    public void setIdVente(long idVente) {
        this.idVente = idVente;
    }

    public long getQteVdu() {
        return qteVdu;
    }

    public void setQteVdu(long qteVdu) {
        this.qteVdu = qteVdu;
    }

    public Date getDateVente() {
        return dateVente;
    }

    public void setDateVente(Date dateVente) {
        this.dateVente = dateVente;
    }

    public float getMontantVente() {
        return montantVente;
    }

    public void setMontantVente(float montantVente) {
        this.montantVente = montantVente;
    }

    public String getRdateVente() {
        return RdateVente;
    }

    public void setRdateVente(String RdateVente) {
        this.RdateVente = RdateVente;
    }
    @Override
    public String toString() {
        return "Vente{" + "idVente=" + idVente + ", qteVdu=" + qteVdu + ", dateVente=" + dateVente + ", montantVente=" + montantVente + '}';
    }
    
}
